# Morteza Karimi Resume

My resume base on [Software developer resume in Latex](https://github.com/sb2nov/resume)

You can see my resume in following links:

* [English Version](https://github.com/mortezakarimi/morteza-karimi-resume/blob/master/morteza_karimi_resume_en.pdf)
* [Persian Version](https://github.com/mortezakarimi/morteza-karimi-resume/blob/master/morteza_karimi_resume_fa.pdf)

## License
Format is MIT but all the data is owned by Morteza Karimi.
